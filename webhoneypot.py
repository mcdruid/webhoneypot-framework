#!/usr/bin/env python3

import argparse
import shlex
import time

from config.config import *
from config.detection import *

from libs.detection import *
from libs.snapshots import *

config = Config()


def get_snapshot_suffix():
    return time.strftime('%Y%m%d_%H%M%S')


def start_honeypot():
    global config

    if config.get_pre_start_script():
        subprocess.call(['/bin/bash', shlex.quote(config.get_pre_start_script())],
                        cwd=config.get_base_directory())

    docker_compose_file = config.get_docker_compose_file()
    logging.debug('Starting docker compose %s ...', docker_compose_file)
    command = 'docker-compose -f %s up -d'
    subprocess.call(command % shlex.quote(docker_compose_file), shell=True)

    if config.get_post_start_script():
        subprocess.call(['/bin/bash', shlex.quote(config.get_post_start_script())],
                        cwd=config.get_base_directory())


def stop_honeypot():
    global config

    docker_compose_file = config.get_docker_compose_file()
    logging.debug('Stopping docker compose %s ...', docker_compose_file)
    command = 'docker-compose -f %s down -v'
    subprocess.call(command % shlex.quote(docker_compose_file), shell=True)


def init_honeypot():
    global config

    snapshot_path = config.get_snapshot_path()

    # preparing environment
    if not os.path.exists(snapshot_path):
        os.mkdir(snapshot_path)

    # starting honeypot
    start_honeypot()

    # waiting until honeypot preparation is finished
    input('Press any key if honeypot setup is finished')

    # create initial snapshot
    create_snapshot(snapshot_path, 'initial', config.get_snapshot_configs())
    # stop_honeypot()


def update_honeypot():
    global config

    snapshot_path = config.get_snapshot_path()

    # reset honeypot
    stop_honeypot()
    restore_snapshot(snapshot_path, 'initial', config.get_snapshot_configs())
    start_honeypot()

    # rename old initial snapshot
    rename_snapshot(snapshot_path, 'initial', 'initial_%s' % get_snapshot_suffix())

    # waiting until honeypot update is finished
    input('Press any key if honeypot setup is finished')

    # create initial snapshot
    create_snapshot(snapshot_path, 'initial', config.get_snapshot_configs())


def reset_honeypot():
    global config

    # snapshot_name = '%d' % int(time.time())
    snapshot_name = get_snapshot_suffix()
    snapshot_path = config.get_snapshot_path()

    # create snapshot
    create_snapshot(snapshot_path, snapshot_name, config.get_snapshot_configs())
    stop_honeypot()

    # create snapshot logger
    file_handler = logging.FileHandler('%s/%s/snapshot.log' % (snapshot_path, snapshot_name))
    file_handler.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
    snapshot_logger = logging.getLogger('snapshot_logger')
    snapshot_logger.addHandler(file_handler)
    snapshot_logger.setLevel(logging.DEBUG)

    # check if a possible compromise was detected
    changed = False
    for detection in config.get_detection_configs():
        if detection.get_type() == 'mysql_db_changed':
            mysql_changed_detection: MySqlSnapshotChanged = detection
            mysql_container: str = mysql_changed_detection.get_mysql_container_name()

            if mysql_snapshot_changed(snapshot_path,
                                      mysql_container,
                                      snapshot_name):
                snapshot_logger.info('MySQL database changed!')
                changed = True
        elif detection.get_type() == 'folder_changed':
            folder_changed_detection: FolderSnapshotChanged = detection

            folder_path = folder_changed_detection.get_folder_path()
            ignore_files = folder_changed_detection.get_ignore_files()

            if folder_snapshot_changed(snapshot_path,
                                       folder_path,
                                       ignore_files,
                                       snapshot_name,
                                       snapshot_logger):
                logging.info('Folder changed!')
                changed = True
        elif detection.get_type() == 'file_contains':
            file_contains_detection: FileContains = detection

            if file_contains(detection.get_file_path(), file_contains_detection.get_pattern()):
                snapshot_logger.info('Pattern "%s" detected in file!', file_contains_detection.get_pattern())
                changed = True

    snapshot_logger.removeHandler(file_handler)

    # close snapshot logger file
    file_handler.close()

    if not changed:
        # no compromise detected -> remove snapshot
        delete_snapshot(snapshot_path, snapshot_name)

    # restore snapshot
    restore_snapshot(snapshot_path, 'initial', config.get_snapshot_configs())
    start_honeypot()


def main():
    global config

    logging.basicConfig(
        level=logging.DEBUG,
        format='%(asctime)s - %(levelname)s - %(message)s',
        handlers=[
            logging.FileHandler('./webhoneypot.log'),
            logging.StreamHandler()
        ])

    parser = argparse.ArgumentParser(description='Management script for docker-based web application honeypots')
    parser.add_argument('-c', '--config', help='the honeypot configuration file', required=True)
    parser.add_argument('option', help='the honeypot option, can be [start|stop|init|update|reset]')
    args = parser.parse_args()

    try:
        config.load(args.config)
    except (FileNotFoundError, ValueError):
        logging.error('Could not load config file "%s"', args.config)
        exit()

    if args.option == 'start':
        start_honeypot()
    elif args.option == 'stop':
        stop_honeypot()
    elif args.option == 'init':
        init_honeypot()
    elif args.option == 'update':
        update_honeypot()
    elif args.option == 'reset':
        reset_honeypot()
    else:
        logging.error('Unknown option "%s"', args.option)
        exit()


if __name__ == '__main__':
    main()
