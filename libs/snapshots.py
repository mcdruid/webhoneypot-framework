import logging
import os
import shutil
import shlex
import subprocess

from typing import List

from config.snapshot import Snapshot, MySqlSnapshot, FolderSnapshot


def create_mysql_snapshot(mysql_container_name: str, mysql_user: str, mysql_password: str, backup_path: str):
    logging.debug('Create database dump of "%s" to "%s" ...', mysql_container_name, backup_path)
    command = 'docker exec %s sh -c "exec mysqldump --all-databases -u%s -p%s" > "%s"'
    subprocess.call(command % (shlex.quote(mysql_container_name),
                               shlex.quote(mysql_user),
                               shlex.quote(mysql_password),
                               shlex.quote(backup_path)), shell=True)


def create_folder_snapshot(folder_path: str, backup_path: str):
    logging.debug('Create folder backup of "%s" to "%s" ...', folder_path, backup_path)
    shutil.make_archive(backup_path, 'zip', folder_path)


def create_snapshot(snapshot_path: str, snapshot_name: str, snapshot_config: List[Snapshot]):
    logging.debug('Creating snapshot "%s" ...', snapshot_name)

    snapshot_path = '%s/%s/' % (snapshot_path, snapshot_name)
    if not os.path.exists(snapshot_path):
        os.mkdir(snapshot_path)

    for action in snapshot_config:
        if action.get_type() == 'mysql_snapshot':
            mysql_snapshot: MySqlSnapshot = action

            # create a mysql database dump
            mysql_container_name = mysql_snapshot.get_mysql_container_name()
            create_mysql_snapshot(mysql_container_name,
                                  mysql_snapshot.get_mysql_user(),
                                  mysql_snapshot.get_mysql_password(),
                                  snapshot_path + mysql_container_name + '.sql')
        elif action.get_type() == 'folder_snapshot':
            folder_snapshot: FolderSnapshot = action

            # create a simple folder snapshot
            folder_path = folder_snapshot.get_folder_path()
            create_folder_snapshot(folder_path, snapshot_path + folder_path.replace('/', '_'))


def restore_mysql_snapshot(backup_path: str, restore_path: str):
    logging.debug('Restore database dump "%s" ...', backup_path)
    shutil.copy(backup_path, restore_path)


def restore_folder_snapshot(folder_path: str, backup_path: str):
    logging.debug('Restore folder backup from "%s" to "%s" ...', backup_path, folder_path)

    # ensure folder is empty
    shutil.rmtree(folder_path, ignore_errors=True)
    os.makedirs(folder_path, exist_ok=True)

    # restore backup folder
    shutil.unpack_archive(backup_path + '.zip', folder_path, 'zip')


def restore_snapshot(snapshot_path: str, snapshot_name: str, snapshot_config: List[Snapshot]):
    logging.debug('Restoring snapshot "%s" ...', snapshot_name)
    snapshot_path = '%s/%s/' % (snapshot_path, snapshot_name)

    for action in snapshot_config:
        if action.get_type() == 'mysql_snapshot':
            mysql_snapshot: MySqlSnapshot = action

            # restore mysql database dump
            mysql_container_name = mysql_snapshot.get_mysql_container_name()
            restore_mysql_snapshot(snapshot_path + mysql_container_name + '.sql',
                                   mysql_snapshot.get_mysql_restore_path())
        elif action.get_type() == 'folder_snapshot':
            folder_snapshot: FolderSnapshot = action

            # restore a simple folder snapshot
            folder_path = folder_snapshot.get_folder_path()
            restore_folder_snapshot(folder_path, snapshot_path + folder_path.replace('/', '_'))


def delete_snapshot(snapshot_path: str, snapshot_name: str):
    snapshot_path = '%s/%s/' % (snapshot_path, snapshot_name)
    shutil.rmtree(snapshot_path, ignore_errors=True)


def rename_snapshot(snapshot_path: str, old_snapshot_name: str, new_snapshot_name: str):
    old_snapshot_path = '%s/%s/' % (snapshot_path, old_snapshot_name)
    new_snapshot_path = '%s/%s/' % (snapshot_path, new_snapshot_name)

    os.rename(old_snapshot_path, new_snapshot_path)
