import logging
import filecmp
import zipfile
import os.path
import re

from typing import List


def mysql_snapshot_changed(snapshot_path: str, mysql_container_name: str, snapshot_name: str) -> bool:
    ref_snapshot = '%s/initial/%s.sql' % (snapshot_path, mysql_container_name)
    cur_snapshot = '%s/%s/%s.sql' % (snapshot_path, snapshot_name, mysql_container_name)

    if not os.path.isfile(ref_snapshot) or not os.path.isfile(cur_snapshot):
        return False

    return not filecmp.cmp(ref_snapshot, cur_snapshot)


def folder_snapshot_changed(snapshot_path: str, folder_path: str, ignore_files: List[str], snapshot_name: str, logger: logging.Logger) -> bool:
    file_name = folder_path.replace('/', '_')
    ref_snapshot = '%s/initial/%s.zip' % (snapshot_path, file_name)
    cur_snapshot = '%s/%s/%s.zip' % (snapshot_path, snapshot_name, file_name)

    if not os.path.isfile(ref_snapshot) or not os.path.isfile(cur_snapshot):
        return False

    return not zip_cmp(ref_snapshot, cur_snapshot, ignore_files, logger)


def file_contains(file_path: str, pattern: str) -> bool:
    if not os.path.isfile(file_path):
        return False

    with open(file_path) as f:
        regex = re.compile(pattern)
        if regex.search(f.read()):
            return True
    
    return False


def file_is_ignored(file_name: str, ignore_files: List[str]) -> bool:
    for ignore_file in ignore_files:
        regex = re.compile(ignore_file)
        if regex.search(file_name):
            return True

    return False


def zip_cmp(ref_file: str, cur_file: str, ignore_files: List[str], logger: logging.Logger) -> bool:
    ref_file = zipfile.ZipFile(ref_file)
    cur_file = zipfile.ZipFile(cur_file)

    ref_file_list = ref_file.namelist()
    cur_file_list = cur_file.namelist()

    all_files = list(set(ref_file_list + cur_file_list))

    for file_name in all_files:
        if not file_is_ignored(file_name, ignore_files):
            if file_name not in ref_file_list:
                logger.info('File created: %s', file_name)
                return False
            elif file_name not in cur_file_list:
                logger.info('File deleted: %s', file_name)
                return False
            else:
                ref_info = ref_file.getinfo(file_name)
                cur_info = cur_file.getinfo(file_name)

                if ref_info.CRC != cur_info.CRC:
                    # file modified
                    logger.info('File changed: %s', file_name)
                    return False

    return True
