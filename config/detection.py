from typing import List


class Detection:
    def __init__(self, type: str):
        self.__type = type

    def get_type(self) -> str:
        return self.__type


class MySqlSnapshotChanged(Detection):
    def __init__(self, mysql_container_name: str):
        Detection.__init__(self, 'mysql_snapshot_changed')

        self.mysql_container_name = mysql_container_name

    def get_mysql_container_name(self) -> str:
        return self.__mysql_container_name


class FolderSnapshotChanged(Detection):
    def __init__(self, folder_path: str, ignore_files: List[str]):
        Detection.__init__(self, 'folder_snapshot_changed')

        self.__folder_path = folder_path
        self.__ignore_files = ignore_files

    def get_folder_path(self) -> str:
        return self.__folder_path

    def get_ignore_files(self) -> List[str]:
        return self.__ignore_files


class FileContains(Detection):
    def __init__(self, file_path: str, pattern: str):
        Detection.__init__(self, 'file_contains')

        self.__file_path = file_path
        self.__pattern = pattern

    def get_file_path(self) -> str:
        return self.__file_path

    def get_pattern(self) -> str:
        return self.__pattern
