class Snapshot:
    def __init__(self, type: str):
        self.__type = type

    def get_type(self) -> str:
        return self.__type


class FolderSnapshot(Snapshot):
    def __init__(self, folder_path: str):
        Snapshot.__init__(self, 'folder_snapshot')

        self.__folder_path = folder_path

    def get_folder_path(self) -> str:
        return self.__folder_path


class MySqlSnapshot(Snapshot):
    def __init__(self, mysql_container_name: str, mysql_user: str, mysql_password: str, mysql_restore_path: str):
        Snapshot.__init__(self, 'mysql_snapshot')

        self.__mysql_container_name = mysql_container_name
        self.__mysql_user = mysql_user
        self.__mysql_password = mysql_password
        self.__mysql_restore_path = mysql_restore_path

    def get_mysql_container_name(self) -> str:
        return self.__mysql_container_name

    def get_mysql_user(self) -> str:
        return self.__mysql_user

    def get_mysql_password(self) -> str:
        return self.__mysql_password

    def get_mysql_restore_path(self) -> str:
        return self.__mysql_restore_path
